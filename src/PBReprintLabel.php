<?php 


namespace FullCycle\Shipping;

use PitneyBowes\PBShippingApi\PBShippingShipment;
use PitneyBowes\PBShippingApi\PBShippingApiError;
use FullCycle\Shipping\Exceptions\FullCycleShippingException;


class PBReprintLabel extends PBShippingLabel {
    protected $shipmentId;
    
    function __construct($shipmentId) {
        parent::__construct();
        $this->shipmentId = $shipmentId;
    }
    
    protected function _make($shipment_id = false) {
        if ($shipment_id)
            $this->shipmentId = $shipment_id;
        $this->shipment = new PBShippingShipment(['shipmentId'=>$this->shipmentId]);
        return $this;
    }
    
    function buy($rate = false) {
        if ($this->shipment) {
            try {
                $this->shipment->reprintLabel($this->getAuthObj(), true);
            } catch (PBShippingApiError $e) {
                /*
                 echo "Create error\n";
                 echo $e->getHttpBody();
                 echo "\nError info\n";
                 print_r( $e->getErrorInfo());
                 echo "Shipment: \n";
                 print_r($this->shipment);
                 */
                throw new FullCycleShippingException("Buy error: {$e->getMessage()}",$e,$this->shipment);
            }
        }
        return $this;
    }
}
    
