<?php

namespace FullCycle\Shipping;

use FullCycle\Shipping\Exceptions\FullCycleShippingMissingConfigException;
use PitneyBowes\PBShippingApi\PBShipping;

class PBConfig {
    
    const LABEL_TYPE_SBRL = "SBRL";
    const LABEL_TYPE_USPS = "USPS";
    const LABEL_TYPE_NEWGISTICS = "NEWGISTICS";
    const LABEL_TYPE_PB_PRESORT = "PBRESORT";
    const LABEL_TYPE_PMOD = "PMOD";
    const MERCHANT_MODE_INDIVIDUAL = "INDIVIDUAL";
    const MERCHANT_MODE_BULK = "BULK";
    const POSTAGE_TYPE_FIRST_CLASS = "FCM";         // First Class Mail
    const POSTAGE_TYPE_PRIORITY = "PM";             // Priority Mail
    const POSTAGE_TYPE_PRCLSEL  = "PRCLSEL";        // Parcel Select
    
    static protected $api_key = false; 
    static protected $api_secret = false; 
    static protected $developerId = false; 
    static protected $shipperId = false; 
    static protected $merchant_email = false; 
    static protected $weight_units = 'OZ';
    static protected $size_units = 'IN';
    static protected $labelType = "SBRL";
    static protected $merchant_mode = self::MERCHANT_MODE_INDIVIDUAL;
    static protected $postageType = "FCM";
    static protected $minimal_address_validation = false;
    
    static function setPostageType($pt) {
        self::$postageType = $pt;
    }
    
    static function getPostageType() {
        return self::$postageType;
    }
    
    static function setToProduction() {
        PBShipping::$is_production=true;
    }
    
    static function setToSandBox() {
        PBShipping::$is_production=false;
    }
    
    static function enableMinimalAddressValidation() {
        self::$minimal_address_validation = true;
    }
    
    static function disableMinimalAddressValidation() {
        self::$minimal_address_validation = false;
    }
    
    static function useMinimalAddressValidation() {
        return self::$minimal_address_validation;
    }
    static function setApiKey($key) {
        self::$api_key = $key;
    }
    
    static function setMerchantMode($mode) {
        self::$merchant_mode = $mode;
    }
    
    static function getMerchantMode() {
        return self::$merchant_mode;
    }
    
    static function setApiSecret($secret) {
        self::$api_secret = $secret;
    }
    
    static function setDeveloperId($id) {
        self::$developerId = $id;
    }
    
    static function setShipperId($id) {
        self::$shipperId = $id;
    }
    
    static function setMerchantEmail($email) {
        self::$merchant_email = $email;
    }
    
    static function getApiKey() {
        if (self::$api_key === false) {
            throw new FullCycleShippingMissingConfigException("Need to set APIKey with PBConfig::setApiKey(key)");
        }
        return self::$api_key;
    }
    
    static function getApiSecret() {
        if (self::$api_secret === false) {
            throw new FullCycleShippingMissingConfigException("Need to set ApiSecret with PBConfig::setApiSecret(key)");
        }
        return self::$api_secret;
    }
    static function getDeveloperId() {
        if (self::$developerId === false) {
            throw new FullCycleShippingMissingConfigException("Need to set DeveloperId with PBConfig::setDeveloperId(key)");
        }
        return self::$developerId;
    }
    static function getShipperId() {
        if (self::$shipperId === false) {
            throw new FullCycleShippingMissingConfigException("Need to set ShipperId with PBConfig::setShipperId(key)");
        }
        return self::$shipperId;
    }
    static function getMerchantEmail() {
        if (self::$merchant_email === false) {
            throw new FullCycleShippingMissingConfigException("Need to set MerchantEmail with PBConfig::setMerchantEmail(key)");
        }
        return self::$merchant_email;
    }
    
    static function getWeightUnits() {
        return self::$weight_units;
    }
    
    static function getSizeUnits() {
        return self::$size_units;
    }
    
    static function setWeightUnits($unit) {
        self::$weight_units = $unit;
    }
    static function setSizeUnits($unit) {
        self::$size_units = $unit;
    }
    
    static function setLabelType($type) {
        self::$labelType = $type;
    }
    
    static function getLabelType() {
        return self::$labelType;
    }
}

