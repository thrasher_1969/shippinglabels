<?php

namespace FullCycle\Shipping;

use FullCycle\Shipping\Exceptions\FullCycleShippingException;
use FullCycle\Shipping\Exceptions\FullCycleShippingAddressException;

use FullCycle\Shipping\PBConfig;
use PitneyBowes\PBShippingApi\PBShipping;
use PitneyBowes\PBShippingApi\PBShippingCarrier;
use PitneyBowes\PBShippingApi\PBShippingAccount;
use PitneyBowes\PBShippingApi\PBShippingShipment;
use PitneyBowes\PBShippingApi\PBShippingTracking;
use PitneyBowes\PBShippingApi\PBShippingApiError;
use PitneyBowes\PBShippingApi\PBShippingManifest;
use PitneyBowes\PBShippingApi\PBShippingRender;
use FullCycle\Shipping\PitneyBowesLabel;

function get_pb_tx_id() {
    return gmdate("YmdHis") . substr((string) microtime(), 2, 7);
}

class PBReturnLabel extends PBShippingLabel {
    
    protected $rates = false;
    protected $merchant = false;
    protected $shimpment_document = false;
    protected $rate_request = false;
    protected $opitons = [];
    protected $postageType;
    
    function __construct($to_address, $from_address, $parcel, array $options=[]) {
        parent::__construct();
        $this->setToAddress($to_address);
        $this->setFromAddress($from_address);
        $this->parcel = $parcel;
        $this->init();
    }
    
    function init() {
        $this->shipperId = PBConfig::getShipperId();
        $this->merchant_email = PBConfig::getMerchantEmail();
        $this->setPostageType(PBConfig::getPostageType());
    }
    
    function setPostageType($pt) {
        $this->postageType = $pt;
    }
    
    function setParcel($parcel) {
        $this->parcel = $parcel;
    }
    
    public function setOptions($options) {
        $this->options = array_merge($this->options,$options);
    }
    
    
    function setTimeOut($ms) {
        
    }
    
    
    function setFromAddress($from_address) {
        $this->from_address = $this->convertToPBAddress($from_address);
    }
    
    function setToAddress($to_address) {
        $this->to_address = $this->convertToPBAddress($to_address);
    }
    
    
    function fetchMerchant() {
        // Need to decide a mode...
        
        $mode = PBConfig::getMerchantMode();
        try {
            if ($mode == PBConfig::MERCHANT_MODE_INDIVIDUAL)
                return $this->fetchIndividualMerchant();
            else if ($mode == PBConfig::MERCHANT_MODE_BULK)
                return $this->fetchBulkMerchant();
            else
                throw new FullCycleShippingException("Invalid Merchant Mode");
        } catch (PBShippingApiError $e) {
           
            echo "Fetch Merchant error: {$e->getMessage()}\n";
            echo $e->getHttpBody();
            print_r( $e->getErrorInfo());
            
            throw new FullCycleShippingException("Fetch Merchant Error",$e);
        }
    }
    
    function fetchIndividualMerchant() {
            $this->merchant = $this->developer->registerMerchantIndividualAccount($this->getAuthObj(), $this->merchant_email);
    }
    
    function fetchBulkMerchant() {
        try {
            $this->merchant = $this->developer->getMerchantBulkAccount($this->getAuthObj(), $this->merchant_email);
        } catch (PBShippingApiError $e) {
            $this->merchant = $this->registerBulkMerchant();
        }
        return $this->merchant;
    }
    
    function registerBulkMerchant(array $merchant_address=[]) {
        if (empty($merchant_address)) {
            $merchant_address = $this->to_address;
            $merchant_address['email'] = $this->merchant_email;
        }
        try {
            $this->merchant = $this->developer->registerMerchantBulkAccount($this->getAuthObj(), $merchant_address);
        } catch (PBShippingApiError $e) {
            /*
            echo "<pre>";
            echo "Bulk merchant error\n";
            echo $e->getHttpBody();
            print_r( $e->getErrorInfo());
            print_r($merchant_address);
            die();
            */
            throw $e;
           //  throw new FullCycleShippingException("Register Bulk Merchant error",$e);
        }
        return $this->merchant;
    }
    
    function fetchMerchants() {
        return $this->developer->getMerchants($this->getAuthObj());
    }
    
    function fetchRender($address) {
        $render = new PBShippingRender();
        return $render->render($this->getAuthObj(),$address);
    }
    
    function getPaymentAcountNumber() {
        if (!$this->merchant)
               $this->fetchMerchant();
        return $this->merchant["paymentAccountNumber"];
    }
    
    function getPostalReportingNumber() {
        if (!$this->merchant)
            $this->fetchMerchant();
        return $this->merchant["postalReportingNumber"];
    }
    
    function getShipmentDocument() {
        if ($this->shimpment_document)
            return $this->shimpment_document;
        return array(      
            "type" => "SHIPPING_LABEL",
            "contentType" => "URL",
            "size" => "DOC_8X11",
            "fileFormat" => "PDF",
            "printDialogOption" => "EMBED_PRINT_DIALOG"
        );
    }
    
    function getPMRateRequest() {
        $rate = array(
            "carrier" => "usps",
            "serviceId" => "PM",
            "parcelType" => "PKG",
        );
        $rate['specialServices'] = [   // Must have a minimum of one special service..  Delivery Confirmation is free.
            [
                "specialServiceId" => "DelCon",
                "inputParameters" => [
                    ["name" => "INPUT_VALUE", "value" => "0"]
                ]
            ]
        ];
        return $rate;
    }
    
    function getFCMRateRequest() {
        $rate = array(
            "carrier" => "usps",
            "serviceId" => "FCM",
            "parcelType" => "PKG",
        );
        
        $rate['specialServices'] = [   // Must have a minimum of one special service..  Delivery Confirmation is free.
            [
                "specialServiceId" => "DelCon",
                "inputParameters" => [
                    ["name" => "INPUT_VALUE", "value" => "0"]
                ]
            ]
        ];
        
        return $rate;
    }
    
    
    function getSTDPOSTRateRequest() {
        return array(
            "carrier" => "usps",
            "serviceId" => "STDPOST",
            "parcelType" => "PKG",
            "specialServices" => [   // Must have a minimum of one special service..  Delivery Confirmation is free.
                [
                    "specialServiceId" => "DelCon",
                    "inputParameters" => [
                        ["name" => "INPUT_VALUE", "value" => "0"]
                    ]
                ]
            ],
        );
        
    }
   
    function getEMRateRequest() {
        return array(
            "carrier" => "usps",
            "serviceId" => "EM",
            "parcelType" => "PKG",
            "specialServices" => [   // Must have a minimum of one special service..  Delivery Confirmation is free.
                [
                    "specialServiceId" => "DelCon",
                    "inputParameters" => [
                        ["name" => "INPUT_VALUE", "value" => "0"]
                    ]
                ]
            ],
        );
    }
    
    
    
    function getPRCLSELRateRequest() {
        return array(
            "carrier" => "usps",
            "serviceId" => "PRCLSEL",
            "parcelType" => "PKG",
            "specialServices" => [   // Must have a minimum of one special service..  Delivery Confirmation is free.
                [
                    "specialServiceId" => "DelCon",
                    "inputParameters" => [
                        ["name" => "INPUT_VALUE", "value" => "0"]
                    ]
                ]
            ],
        );
    }
    
    function getRateRequest() {
        if ($this->rate_request)
            return $this->rate_request;
        return $this->getEMRateRequest();        
    }
    
    function getRatesRequest( $carrier = false) {
        if (!$carrier)
            return array($this->getRateRequest());
        $rates = [];
        $method="get" . $carrier . "RateRequest";
        if (method_exists($this,$method)) {
            $rates[] = $this->$method();
        } else 
            throw new \Exception("Invalid Rate request type");
        return $rates;
    }
        
    function updateRates() {
        if (!$this->shipment)
            return false;
        try {
            $this->rates = $this->shipment->getRates($this->getAuthObj(), get_pb_tx_id(), true);
        } catch (PBShippingApiError $e) {
            echo $e->getHttpBody();
            throw $e;
        }
        
    }
    
    function getRates() {
        if ($this->rates)
            return $this->rates;
        $this->updateRates();
        return $this->rates;    
    }
    
    
    protected function _make($to_address = false, $from_address = false, $parcel = false, array $options=[]) {
        if ($to_address)
            $this->setToAddress($to_address);
        if ($from_address)
            $this->setFromAddress($from_address);
        if ($parcel)
            $this->setParcel($parcel);
        if (!empty($options))
            $this->setOptions($options);
        $this->verifyAddress($this->from_address);
        $this->verifyAddress($this->to_address);
        
       $this->shipment = new PBShippingShipment(array(
           "fromAddress" => $this->from_address,
           "toAddress" => $this->to_address,
           "parcel" => $this->parcel,
//           "rates" => $this->getRatesRequest('FCM'),
           "rates" => $this->getRatesRequest(PBConfig::getPostageType()),
       ));
       return $this;
    }
    
    public function lowest_rate() {
        if (!$this->rates)
            $this->updateRates();
        $lowest = $this->rates[0];
        foreach ($this->rates as $rate) {
            if ($lowest['totalCarrierCharge'] > $rate['totalCarrierCharge'])
                $lowest = $rate;
        }
        return $lowest;
    }
       
   public function getShipment() {
       return $this->shipment;
   }
   
   public function addShipmentOption($name, $value) {
       $this->shipment["shipmentOptions"][] = [
           "name" => $name,
           "value" => $value,
       ];
       return $this;
   }
   
   function addCustomMessage($msg) {
       $this->addShipmentOption('PRINT_CUSTOM_MESSAGE_1',substr($msg,0,50));
       return $this;
   }
   
   protected function addValuesToShipment() {
       $this->addShipmentOption("SHIPPER_ID",$this->getPostalReportingNumber());
       /*
       $this->shipment["shipmentOptions"] = [
           ["name" => "SHIPPER_ID", "value" => $this->getPostalReportingNumber()],
           //               ["name" => "ADD_TO_MANIFEST", "value" => true]         // Not for return labels.
       ];
       */
       $this->shipment["documents"] = array($this->getShipmentDocument());
       if ($this->minimal_address_validate) {
           $this->addShipmentOption('MINIMAL_ADDRESS_VALIDATION',true);
           /*
           $this->shipment["shipmentOptions"][] = [
               'name' => 'MINIMAL_ADDRESS_VALIDATION',
                'value' =>true,
            ];
            */
       }
       /*
       $this->shipment["shipmentOptions"][] = [
           'name' => 'PRINT_CUSTOM_MESSAGE_1',
           'value' => "My custom message"
       ];
       */
       return $this;
   }
       
   function buy($rate = false) {
       if ($this->shipment) {
           if (!$rate) {
                $rate = $this->lowest_rate();
           }
           $shipment_orig_tx_id = get_pb_tx_id();
           $this->shipment['rates'] = [$rate];
//           $this->shipment['shipmentType'] = 'RETURN';
           $this->addValuesToShipment();
           try {
               $this->shipment->createAndPurchase($this->getAuthObj(), $shipment_orig_tx_id, true);
           } catch (PBShippingApiError $e) {
               /*
               echo "Create error\n";
               echo $e->getHttpBody();
               echo "Error info\n";
               print_r( $e->getErrorInfo());
               echo "Shipment: \n";
               print_r($this->shipment);
               */
               throw new FullCycleShippingException("Buy error: {$e->getMessage()}",$e,$this->shipment);
           }
       }
       return $this;
   }
   
}

