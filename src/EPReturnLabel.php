<?php 

namespace FullCycle\Shipping;


class EPReturnLabel {
    
    protected $api_key = "EZTK6076429c765a4b678cdd563c73ee53295uqFMycfF6ScgcEQdLAd0A";
    protected $to_address;
    protected $from_address;
    protected $parcel;
    protected $shipment;
    
    function __construct($to_address, $from_address, $parcel) {
        $this->to_address = $to_address;
        $this->from_address = $from_address;
        $this->parcel = $parcel;
        \EasyPost\EasyPost::setApiKey($this->api_key);
    }
    
    function setParcel($parcel) {
        $this->parcel = $parcel;
    }
    
    function setTimeOut($ms) {
        \EasyPost\EasyPost::setConnectTimeout($ms);
        \EasyPost\EasyPost::setResponseTimeout($ms);
        
        
    }
    function setFromAddress($from_address) {
        $this->from_address = $from_address;
    }
    
    function setToAddress($to_address) {
        $this->to_address = $to_address;
    }
    
    function make($to_address = false, $from_address = false, $parcel = false) {
        $this->setTimeOut(5000);
        $easyToAddress = \EasyPost\Address::create($this->to_address);
        $easyFromAddress = \EasyPost\Address::create($this->from_address);
        $easyParcel = \EasyPost\Parcel::create($this->parcel);
        $params = [
            "to_address"   => $easyToAddress,
            "from_address" => $easyFromAddress,
            "parcel"       => $easyParcel,
        ];
        $this->shipment = \EasyPost\Shipment::create($params);
        return $this;
    }
    
    function buy($rate = false) {
        if ($this->shipment) {
            if ($rate)
                $this->shipment->buy($rate);
            else 
                $this->shipment->buy($this->shipment->lowest_rate());
        }
        return $this;
    }
    
    function insure($amount) {
        if ($this->shipment)
            $this->shipment->insure(['amount' => $amount]);
        return $this;
    }
    
    public function lowest_rate() {
        if ($this->shipment)
            return $this->shipment->lowest_rate();
        return false;
    }
    
    public function getShipment() {
        return $this->shipment;
    }
    
}


?>
