<?php
namespace FullCycle\Shipping;

class PBShippingLabel extends PitneyBowesLabel {
    
    protected $shipment;
    
    public function getShipment() {
        return $this->shipment;
    }
    
    static function make(...$args) {
        $obj = new static(...$args);
        $obj->_make();
        return $obj;
    }
    
    function getReturnLabelURL() {
        $url = false;
        foreach($this->shipment["documents"] as $doc) {
            if (strtolower($doc["contentType"]) == "url" && isset($doc["contents"])) {
                $url = $doc["contents"];
            }
        }
        return $url;
    }
    function getTrackingNumber() {
        return $this->shipment['parcelTrackingNumber'];
    }
}

