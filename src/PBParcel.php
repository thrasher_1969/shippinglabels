<?php

namespace FullCycle\Shipping;

/**
 * 
 * @author thrasher
 *
 * The dimension isn't complete and must follow the settings from the PB docs, bet generally this isn't required.
 *
 */

class PBParcel extends \ArrayObject {
        public $weight;
        function __construct($weight, $dimension = false) {
            $this->setWeight($weight);
            if ($dimension)
                  $this->setDimension($dimension);
        }
        
        function setWeight($weight) {
            $this['weight'] = [
                "unitOfMeasurement" => PBConfig::getWeightUnits(),
                "weight" => $weight,
            ];
        }
        
        function setDimension($dimension) {
            $this['dimension'] = $dimension;
        }
}



