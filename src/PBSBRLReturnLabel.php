<?php 


namespace FullCycle\Shipping;

class PBSBRLReturnLabel extends PBReturnLabel {
    
    function __construct($to_address, $from_address, $parcel) {
        parent::__construct($to_address,$from_address,$parcel);
    }
    
    
    function getPMRateRequest() {
        $rate = array(
            "carrier" => "usps",
            "serviceId" => "PM",
            "parcelType" => "PKG",
        );
        return $rate;
    }
    
    function getFCMRateRequest() {
        $rate = array(
            "carrier" => "usps",
            "serviceId" => "FCM",
            "parcelType" => "PKG",
        );        
        return $rate;
    }
    function getPRCLSELRateRequest() {
        return array(
            "carrier" => "usps",
            "serviceId" => "PRCLSEL",
            "parcelType" => "PKG",
            "specialServices" => [],
        );
    }
    
    
    
    protected function _make($to_address = false, $from_address = false, $parcel = false,array $options=[]) {
        parent::_make($to_address,$from_address,$parcel, $options);
        $this->shipment['shipmentType'] = 'RETURN';
        return $this;
    }
    
    public function lowest_rate() {
        if (!$this->shipment)
            throw \Exception("Must first make shipment");
        return $this->shipment['rates'][0];
    }
    
}
    