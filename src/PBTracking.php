<?php

namespace FullCycle\Shipping;

use PitneyBowes\PBShippingApi\PBShippingTracking;


class PBTracking extends PitneyBowesLabel {

    protected $trackingNumber;
    protected $carrier;
    protected $packageIdentifierType;
    
    function __construct($trackingNumber, $carrier = "USPS", $packageIdentifierType = "TrackingNumber") {
		parent::__construct();
		$this->trackingNumber = $trackingNumber;
		$this->carrier = $carrier;
		$this->packageIdentifierType = $packageIdentifierType;
	}

	function setTrackingNumber($number) {
	    $this->trackingNumber = $number;
	}
	
	function get() {
	    $pbTracking = new PBShippingTracking();
	    $pbTracking['trackingNumber'] = $this->trackingNumber;
	    $pbTracking['carrier'] = $this->carrier;
	    $pbTracking['packageIdentifierType'] = $this->packageIdentifierType;
	    $pbTracking->updateStatus($this->getAuthObj());
	    return $pbTracking;
	    
	}
}
