<?php

namespace FullCycle\Shipping\Exceptions;

use  FullCycle\Shipping\Exceptions\FullCycleShippingException;

class FullCycleShippingAddressException extends FullCycleShippingException {}


