<?php

namespace FullCycle\Shipping\Exceptions;


class FullCycleShippingException extends \Exception { 

	function __construct($msg, $exception = false, $data = false) {
		parent::__construct($msg);
		$this->exception = $exception;
		$this->data = $data;
	}

	public function getData() {
		return $this->data;
	}

	public function getException() {
		return $this->exception;
	}
}


