<?php

namespace FullCycle\Shipping\Exceptions;
use FullCycle\Shipping\Exceptions\FullCycleShippingException;

class FullCycleShippingMissingConfigException extends FullCycleShippingException { }


