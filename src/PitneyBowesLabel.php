<?php

namespace FullCycle\Shipping;

use PitneyBowes\PBShippingApi\PBShippingAuthentication;
use PitneyBowes\PBShippingApi\PBShippingDeveloper;
use PitneyBowes\PBShippingApi\PBShippingAddress;


class PitneyBowesLabel {

    protected $auth_obj = false;
    protected $developer = false;
    protected $minimal_address_validate = false;
    
    function __construct() {
        $this->api_key = PBConfig::getApiKey();
        $this->api_secret = PBConfig::getApiSecret();
        $this->developerId = PBConfig::getDeveloperId();
        $this->developer = new PBShippingDeveloper(array("developerId" => $this->developerId));
        if (PBConfig::useMinimalAddressValidation())
            $this->minimal_address_validate = true;
    }
    
    function fetchAuth() {
        try {
            $this->auth_obj = new PBShippingAuthentication($this->api_key, $this->api_secret);
        } catch (\Exception $e) {
            throw $e;
        }
        return $this->auth_obj;
    }
    
    function getAuthObj() {
        if ($this->auth_obj)
            return $this->auth_obj;
        return $this->fetchAuth();
    }
    
    /**
     * converts standard address array to PB address form.
     * @param array $address
     *  address has [address,address2,street,city,state,postal_code,country_code]
     *
     * @return array
     *
     * @TODO validate the fields are set.
     */
    function convertToPBAddress(array $address) {
        
        $addressLines[] = $address['address'];
        if (isset($address['address2']))
            $addressLines[] = $address['address2'];
            $convertedAddress = [
                "addressLines" => $addressLines,
                "cityTown" => $address['city'],
                "stateProvince" => $address['state'],
                "postalCode" => $address['postal_code'],
                "countryCode" => $address['country_code']
            ];
        if (!empty($address['name']))
            $convertedAddress['name'] = $address['name'];
        if (!empty($address['company']))
            $convertedAddress['company'] = $address['company'];
            
        if (!empty($address['email']))
            $convertedAddress['email'] = $address['email'];
        if (!empty($address['phone']))
            $convertedAddress['phone'] = $address['phone'];
            
        return $convertedAddress;
    }
    
    public function verifyAddress(&$address) {
        if (!empty($address['phone']))
            $phone = $address['phone'];
        try {
            PBShippingAddress::verify($this->getAuthObj(), $address, $this->minimal_address_validate);
        } catch (PBShippingApiError $e) {
            /*
             echo "verify address failed\n";
             print_r($address);
             echo "Http body\n";
             echo $e->getHttpBody();
             echo "\n";
             echo "Error info\n";
             print_r( $e->getErrorInfo());
             echo "\nError message\n";
             echo $e->getMessage();
             */
            throw new FullCycleShippingAddressException("Failed to verify address", $e, $address);
        }
        if (!empty($phone)) // Add phone back in since it gets removed.
            $address['phone'] = $phone;
    }
    
    
}


